![](https://codeberg.org/softketchup/split_and_merge_color_channels/raw/branch/main/ap.jpg)


## B-channel  

![](https://codeberg.org/softketchup/split_and_merge_color_channels/raw/branch/main/b.jpg)


## G-channel  

![](https://codeberg.org/softketchup/split_and_merge_color_channels/raw/branch/main/g.jpg)


## R-channel  

![](https://codeberg.org/softketchup/split_and_merge_color_channels/raw/branch/main/r.jpg)