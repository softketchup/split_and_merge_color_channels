# conda activate DCTWM 
# conda activate iWM

# https://pyimagesearch.com/2021/01/23/splitting-and-merging-channels-with-opencv/
# https://ajlearn.net/opencv_split_merge
# https://habr.com/ru/articles/678260/

import numpy as np
import cv2
# pip install --upgrade opencv-python


name = "ap.jpg"

image = cv2.imread(name, 1)

(B, G, R) = cv2.split(image)

# show each channel individually
# B, G и R, разделенные функцией cv2.split, являются одноканальными изображениями.
cv2.imwrite(f"Red_{name[:-4]}.jpg", R)
cv2.imwrite(f"Green_{name[:-4]}.jpg", G)
cv2.imwrite(f"Blue_{name[:-4]}.jpg", B)


R = cv2.imread(f"Red_{name[:-4]}.jpg", 0)
if R is None: 
    print("\nCould not open or find the R\n")
G = cv2.imread(f"Green_{name[:-4]}.jpg", 0)
if G is None: 
    print("\nCould not open or find the G\n")
B = cv2.imread(f"Blue_{name[:-4]}.jpg", 0)
if B is None: 
    print("\nCould not open or find the B\n")

# Функция cv2.merge объединяет один канал в несколько каналов
# (несколько многоканальных изображений нельзя объединить).

merged = cv2.merge([B,G,R])

cv2.imwrite(f"Merged_{name[:-4]}.jpg", merged)